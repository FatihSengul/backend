package com.fsengul.cookingrecipes.model;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "recipes")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "published")
    private boolean published;

    @Column(name = "cre_date")
    private Date cre_date;

    @Column(name = "upd_date")
    private Date upd_date;

    @Column(name = "cre_user")
    private String cre_user;

    public Recipe() {

    }

    public Recipe(String title, String description, boolean published) {
        this.title = title;
        this.description = description;
        this.published = published;
    }

    public Recipe(String title, String description, boolean published, Date cre_date, Date upd_date) {
        this.title = title;
        this.description = description;
        this.published = published;
        this.cre_date = cre_date;
        this.upd_date = upd_date;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean isPublished) {
        this.published = isPublished;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCre_date(Date cre_date) {
        this.cre_date = cre_date;
    }

    public Date getUpd_date() {
        return upd_date;
    }

    public void setUpd_date(Date upd_date) {
        this.upd_date = upd_date;
    }

    public String getCre_user() {
        return cre_user;
    }

    public void setCre_user(String cre_user) {
        this.cre_user = cre_user;
    }

    @Override
    public String toString() {
        return "Tutorial [id=" + id + ", title=" + title + ", desc=" + description + ", published=" + published + "]";
    }

}
