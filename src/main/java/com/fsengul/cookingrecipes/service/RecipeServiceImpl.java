package com.fsengul.cookingrecipes.service;

import com.fsengul.cookingrecipes.model.Recipe;
import com.fsengul.cookingrecipes.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService{
    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

   @Override
   public List<Recipe> getAllRecipe(){
        return recipeRepository.findAll();
   }

  @Override
  public Recipe createRecipe(Recipe recipe){
        recipe.setCre_date(new Date());
        recipe.setCre_user("Fatih Şengül");
        return recipeRepository.save(recipe);
  }

  @Override
  public void deleteRecipe(Long recId){
        recipeRepository.deleteById(recId);
  }

  @Override
  public void deleteAll(){
        recipeRepository.deleteAll();
  }

  @Override
  public Recipe updateRecipe(Long recId, Recipe recipeDetails){
        Recipe recipe = recipeRepository.findById(recId).get();
        recipe.setTitle(recipeDetails.getTitle());
        recipe.setDescription(recipeDetails.getDescription());
        recipe.setPublished(true);
        recipe.setUpd_date(new Date());
        return recipeRepository.save(recipe);
  }

  @Override
  public List<Recipe> getByTitleContaining(String title){
        return recipeRepository.findByTitleContaining(title);

  }

  @Override
  public List<Recipe> getByPublished(boolean published){
        return recipeRepository.findByPublished(published);
  }

 @Override
 public Recipe getByIdRecipe(Long id){
        return recipeRepository.findById(id).get();
 }



}
