package com.fsengul.cookingrecipes.service;

import com.fsengul.cookingrecipes.model.Recipe;

import java.util.List;

public interface RecipeService {
    List<Recipe> getAllRecipe();

    Recipe createRecipe(Recipe recipe);

    void deleteRecipe(Long recId);

    void deleteAll();

    Recipe updateRecipe(Long recId, Recipe recipeDetails);

    List<Recipe> getByTitleContaining(String title);

    List<Recipe> getByPublished(boolean published);

    Recipe getByIdRecipe(Long id);
}
