package com.fsengul.cookingrecipes.repository;

import com.fsengul.cookingrecipes.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findByPublished(boolean published);
    List<Recipe> findByTitleContaining(String title);
}
