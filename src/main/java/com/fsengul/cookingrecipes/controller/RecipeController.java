package com.fsengul.cookingrecipes.controller;

import com.fsengul.cookingrecipes.model.Recipe;
import com.fsengul.cookingrecipes.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class RecipeController {


    @Autowired
    RecipeService recipeService;

    @GetMapping("/recipes")
    public ResponseEntity<List<Recipe>> getAllRecipes(@RequestParam(required = false) String title) {
        try {
            List<Recipe> recipes = new ArrayList<Recipe>();

            if (title == null)
                recipeService.getAllRecipe().forEach(recipes::add);
            else
                recipeService.getByTitleContaining(title).forEach(recipes::add);

            if (recipes.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(recipes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/recipes/{id}")
    public ResponseEntity<Recipe> getRecipeById(@PathVariable("id") long id) {
//        Optional<Recipe> recipeData = recipeRepository.findById(id);
        Optional<Recipe> recipeData = Optional.ofNullable(recipeService.getByIdRecipe(id));

        if (recipeData.isPresent()) {
            return new ResponseEntity<>(recipeData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/recipes")
    public ResponseEntity<Recipe> createRecipe(@RequestBody Recipe recipe) {
        try {
//            Recipe _recipes = recipeRepository
//                    .save(new Recipe(recipe.getTitle(), recipe.getDescription(), false));
//            return new ResponseEntity<>(_recipes, HttpStatus.CREATED);
            Recipe _recipes = recipeService
                    .createRecipe(new Recipe(recipe.getTitle(), recipe.getDescription(), true));
            return new ResponseEntity<>(_recipes, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("/recipes/{id}")
    public ResponseEntity<Recipe> updateRecipe(@PathVariable("id") long id, @RequestBody Recipe recipe) {
//        Optional<Recipe> recipeData = recipeRepository.findById(id);
//
//        if (recipeData.isPresent()) {
//            Recipe _recipe = recipeData.get();
//            _recipe.setTitle(recipe.getTitle());
//            _recipe.setDescription(recipe.getDescription());
//            _recipe.setPublished(recipe.isPublished());
//            return new ResponseEntity<>(recipeRepository.save(_recipe), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        Optional<Recipe> recipeData = Optional.ofNullable(recipeService.getByIdRecipe(id));
//
//        if (recipeData.isPresent()) {
//            Recipe _recipe = recipeData.get();
//            _recipe.setTitle(recipe.getTitle());
//            _recipe.setDescription(recipe.getDescription());
//            _recipe.setPublished(recipe.isPublished());
//            _recipe.setUpd_date(new Date());
//            return new ResponseEntity<>(recipeService.createRecipe(_recipe), HttpStatus.OK);
//        } else {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
        Optional<Recipe> recipeData = Optional.ofNullable(recipeService.getByIdRecipe(id));

        if (recipeData.isPresent()) {
            Recipe _recipe = recipeData.get();
            _recipe.setTitle(recipe.getTitle());
            _recipe.setDescription(recipe.getDescription());
            _recipe.setPublished(recipe.isPublished());
            _recipe.setUpd_date(new Date());
            return new ResponseEntity<>(recipeService.updateRecipe(id,_recipe), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/recipes/{id}")
    public ResponseEntity<HttpStatus> deleteRecipe(@PathVariable("id") long id) {
        try {
            recipeService.deleteRecipe(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @DeleteMapping("/recipes")
    public ResponseEntity<HttpStatus> deleteAllRecipes() {
        try {
            recipeService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }

    }

    @GetMapping("/recipes/published")
    public ResponseEntity<List<Recipe>> findByPublished() {
        try {
            List<Recipe> recipes = recipeService.getByPublished(true);

            if (recipes.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(recipes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

}
